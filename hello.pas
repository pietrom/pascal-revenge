program Hello(input, output);

procedure writeNumberedHello (i : Integer);
begin
	writeln('Hello, ', i);
end;

var
	i: Integer;

begin
  writeln ('Hello, world.');
  for i:= 1 to 20 do
  begin
	writeNumberedHello(i);
  end;
  
  i := 1;
  while i <= 20 do
  begin
	writeNumberedHello(i);
	i := i + 1;
  end;
  
  i := 1;
  repeat
  begin
	writeNumberedHello(i);
	i := i + 1;
  end;
  until i > 20;
end.

program CLParams;

var
  I: Integer;
begin
  WriteLn('Program: ', ParamStr(0));
  WriteLn('Parameters count: ', ParamCount);
  for I := 1 to ParamCount do
    WriteLn('Param ', I, ': ', ParamStr(I));
end.
